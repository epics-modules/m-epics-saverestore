# SaveRestore

**SaveRestore is a support application module that allows to save or restore PV's value at any time.**


## Files

* **db/SaveRestore.template** : Save, restore and saved file path records.
* **db/SeqLoad.template** : Records used for the loading sequence.
* **src/SaveRestore.c** : Asub function calling the python script used to save or restore PV's value. Declare also the command 'SaveRestoreConfigure(arg1,arg2), to registered the path where the script is and the path where the list of PV save is.
* **src/seqFilesDirectory.c** : List in a string waveform the files contains in a directory. 
* **src/SequenceLoad.st** : Sequences of loading. Load one after the other the sav file contains in a directory. A bo indicates that we can go to then next step.  
* **misc/LauncherSaveRestore.py** : Execute with simple option the save_restore_cea.py script.
* **misc/save_restore_cea.py** : Save and restore PV with python. Copy here https://github.com/pyepics/pyepics/blob/master/scripts/save_restore.py but modified to accept '-'
* **misc/example.req** : req example. More information : http://cars9.uchicago.edu/software/python/pyepics3/autosave.html



## Records

* **SaveRestore.template**
    * **$(PREFIX):PathSav"** : Path where is saved or restored the values (*char waveform*)
    * **$(PREFIX):Save** : saving record (1) (*aSub*)
    * **$(PREFIX):PVS"** : restoring record (*char waveform*)
* **SeqLoad.template**
    * **$(PREFIX):Load-Seq"** : To stop or start the sequence (*bo*)
    * **$(PREFIX):Tab-Seq-Size"** : Indicates how many file will be loaded (*ai*)
    * **$(PREFIX):Tab-Seq"** : Indicates the list of the file that will be loaded (*waveform*)
    * **$(PREFIX):Seq-Dir"** : Path of the directory where all the files used for the sequence are (*waveform*)
    * **$(PREFIX):Seq-Dir-Asub"** : Asub that list the files contains in the directory (*aSub*)

  

## Database macros

* **PREFIX** : Any prefix you want   

## Sequence macros

* **defect** : Name of the critical defect that wil stop the sequence (PV)
* **loadfile** : Path of the actual load file(PV)
* **load** :PV triggered to restore values (PV)
* **isSeqStarted** : bo indicating if we can pass to the next step (PV)
* **Experiment** : Prefix of the standard pv (PV)


## How to use the application

On EEE (ESS EPICS Environment)

An IOC example is given in **startup/** 

1/ Modify the record *$(PREFIX):PathSav*  :

    dbpf $(PREFIX):PathSav "my_path"

2/ Process the record **$(PREFIX):Save** to save the value in the path you've just indicated

3/ Process the record **$(PREFIX):Restore** to restore the value from the path you've just indicated