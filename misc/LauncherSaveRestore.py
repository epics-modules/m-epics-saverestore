#!/usr/bin/env python

import save_restore_cea
import argparse
import epics

parser = argparse.ArgumentParser(description='Execute action on the archiver.')
parser.add_argument('--source', metavar='s', nargs='+', help='pvs on format *.req type, see autosave')
parser.add_argument('--action', metavar='A', nargs='+', help='save or restore')
parser.add_argument('--sav', metavar='S', nargs='+', help='format *.sav')


args = parser.parse_args()
if (args.action is not None and len(args.action)>0):
	if(args.action[0] in  "restore"):
		if (args.sav is not None and len(args.sav)>0):
			save_restore_cea.restore_pvs(args.sav[0])
		else:
			print "Error : Indicate a file to restore"
	elif(args.action[0] in  "save"):
		if (args.sav is not None and len(args.sav)>0):
			if (args.source is not None and len(args.sav)>0):
				save_restore_cea.save_pvs(args.source[0],args.sav[0])
			else:
				print "Indicate a file typ *.req like the file for the autosave module"
		else:
			print "Error : Indicate a file where to save pvs values"
	else:
		 print "Error : Actions possibles are restore or save"
else:
	print "Error : Indicate an action, restore or save."
exit(0)



