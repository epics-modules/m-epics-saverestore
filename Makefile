include $(EPICS_ENV_PATH)/module.Makefile

#STARTUPS = startup/SaveRestore.cmd
#DOC = doc/README.md
#USR_DEPENDENCIES = <module>,<version>
PROJECT= SaveRestore
MISCS += misc/LauncherSaveRestore.py
MISCS += misc/save_restore_cea.py
MISCS += misc/example.req

SNCFLAGS += +r