#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <iocsh.h>

#define MAXLINE 80
struct msgBuff {    /* line output structure */
    char            out_buff[MAXLINE + 1];
    char           *pNext;
    char           *pLast;
    char           *pNexTab;
    char            message[128];
};
typedef struct msgBuff TAB_BUFFER;

typedef enum { false = 0, true = !false } bool;

static const char *PathSaveRestorePython=NULL;


static const iocshArg SaveRestoreConfigureArg0 = { "pathPython", iocshArgString };

static const iocshArg * const SaveRestoreConfigureArgs[] = {&SaveRestoreConfigureArg0};

static const iocshFuncDef SaveRestoreConfigureDef = { "SaveRestoreConfigure", 1, SaveRestoreConfigureArgs };

void SaveRestoreConfigure(char* path){
	//Path python
	int i=strlen(path);
	if (PathSaveRestorePython != NULL){
		 free(PathSaveRestorePython);
	}
	PathSaveRestorePython=malloc(i+1);
	strcpy(PathSaveRestorePython,path);



}

static void SaveRestoreConfigureFunc (const iocshArgBuf *args)
{
  SaveRestoreConfigure (args[0].sval);
}




static void SaveRestoreRegister ()
{
  iocshRegister (&SaveRestoreConfigureDef, SaveRestoreConfigureFunc);
  /* iocshRegister (other shell functions) */
}
epicsExportRegistrar (SaveRestoreRegister);




static int saveRestore(aSubRecord *precord) {
	char *PathSav=(char *)precord->a;
	char *PathReq=(char *)precord->b;
   	float save=*(float *)precord->c;


		/* code for child process */
		char *calledPython;
		char *action;
		calledPython="/misc/LauncherSaveRestore.py";
		char pathAux[200];
		if (save==true){
			action= "--action=save";
		}else{
			action= "--action=restore";
		}
    	char command[1024];
		sprintf(command,"python %s%s %s --source=%s --sav=%s",PathSaveRestorePython,calledPython,action,PathReq,PathSav);
		printf("%s\n",command);
		system(command);
	return 0;
}

epicsRegisterFunction(saveRestore);