/*
 * C.E.A. IRFU/SIS/LDISC
 *
 * $Id$
 *
 * who       when      what
 * --------  --------  ----------------------------------------------
 * agaget  15/04/16  created
 */

program SequenceLoad

%%#include <string.h>
%%#include <time.h> 
%%#include <stdlib.h>
%%#include <stdio.h>


/* start/stop/pause button */
int startButton;
assign startButton to "{Experiment}:Load-Seq";
monitor startButton;


int IsSequenceStarted;
assign IsSequenceStarted to "{isSeqStarted}";
monitor IsSequenceStarted;

string tabSeq[256];
assign tabSeq to "{Experiment}:Tab-Seq";
monitor tabSeq;

int seqSize;
assign seqSize to "{Experiment}:Tab-Seq-Size";
monitor seqSize;

int defect;
assign defect to "{defect}";
monitor defect;

int load;
assign load to "{load}";

char loadFile[1024];
assign loadFile to "{loadFile}";

char seqDir[1024];
assign seqDir to "{Experiment}:Seq-Dir";
monitor seqDir;

int indexSeq;
assign indexSeq to "{Experiment}:Seq-Index";



ss ss3
{
    state idle
    {
		when (startButton == 1)
		{  
			
			printf("Start the sequence\n");
			indexSeq=0;
			pvPut(indexSeq);
			sprintf(loadFile,"%s/%s",seqDir,tabSeq[indexSeq]);
			//strcpy(loadFile,tabSeq[indexSeq]);
			printf("load file %s\n",loadFile);
			pvPut(loadFile);
			load=1;
			pvPut(load);
		} state waitForSeq
    }

    state DebutCycle
    {
		//when defect, we stop this sequence and the subsequence
		when (defect == 1)
		{ 
			startButton=0;
			IsSequenceStarted=0;
			pvPut(startButton);	
			pvPut(IsSequenceStarted);	
		} state idle

		when (startButton == 0)
		{  
			printf("Stop in debuCycle state\n");
			IsSequenceStarted=0;
			pvPut(IsSequenceStarted);	     
			
		} state idle

		when (IsSequenceStarted == 0 && indexSeq<seqSize-1)
		{  
			printf("SubSequence just stopped %i %i\n",indexSeq,seqSize);
			indexSeq++;
			pvPut(indexSeq);
			sprintf(loadFile,"%s/%s",seqDir,tabSeq[indexSeq]);
			//strcpy(loadFile,tabSeq[indexSeq]);
			printf("load file %s\n",loadFile);
			pvPut(loadFile);
			load=1;
			pvPut(load);
		} state waitForSeq

		when (IsSequenceStarted == 0 && indexSeq>=seqSize-1)
		{  
			startButton=0;
			pvPut(startButton);

		}state idle
    }
	
	state waitForSeq
    {	
		//when defect, we stop this sequence and the subsequence
		when (defect == 1)
		{ 
			startButton=0;
			IsSequenceStarted=0;
			pvPut(startButton);	
			pvPut(IsSequenceStarted);	
		} state idle

		when (startButton == 0)
		{  
			printf("Stop during wait\n");
			IsSequenceStarted=0;
			pvPut(IsSequenceStarted);	     
			
		} state idle

		when(delay(3)){
			printf("wait 3 seconds\n");
			IsSequenceStarted=1;
			pvPut(IsSequenceStarted);
		}state DebutCycle

	}
}
