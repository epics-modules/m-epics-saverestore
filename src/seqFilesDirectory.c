#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>


typedef enum { false = 0, true = !false } bool;


        
static int seqFilesDirectory(aSubRecord *precord) {
    printf("SeqFilesDirectory\n");
   	char *waveform = (char *)precord->a;
    struct dirent *lecture;
    DIR *rep;
    const char *dir=waveform;
    rep=opendir(dir);
    int i=0;
    //if directory exist
    if (rep!=NULL){
        int j;
        char* sequences=precord->vala;
        
        /*while((lecture=readdir(rep))){
           if (strcmp(lecture->d_name,".")!=0 && strcmp(lecture->d_name,"..")!=0){
            printf("file : %s\n",lecture->d_name);
                //To improve, 40 for the size of the EPICS String
                for (j=0;j<40;j++){
                    sequences[40*i+j]=lecture->d_name[j];
                }
                i++;
                }
        }*/
        struct dirent **namelist;
        int n;

        n= scandir(dir,&namelist,0,alphasort);
        printf("siz %i\n",n);
        if (n<0){
            perror("scandir");
        }
        else{
            int count=0;
            while(count<n){
               if (strcmp(namelist[count]->d_name,".")!=0 && strcmp(namelist[count]->d_name,"..")!=0){
                     printf("file : %s\n",namelist[count]->d_name);
                     //To improve, 40 for the size of the EPICS String
                     for (j=0;j<40;j++){
                            sequences[40*i+j]=namelist[count]->d_name[j];
                     }
                i++;
                } 
                count++;
            }
        }


        precord->neva=i;
        printf("closing dir %s\n",dir);
                if(closedir(rep)==-1){
        printf("error %s dir\n",errno);
        };
        printf("closed dir\n");
    }else{
        printf("Warning : this directory doesn't exist \"%s\"\n",waveform);

    }
	return 0;
}

epicsRegisterFunction(seqFilesDirectory);